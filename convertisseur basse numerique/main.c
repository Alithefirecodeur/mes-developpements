#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define N 100

int power(int a,int b){
    int r= 1;
    for(int i=0;i<b;i++){
        r*=a;
    }
        return r;
}


void affichertab(int nb,int tab[]){
    for(int i=0;i<nb;i++){
        printf(" %d\n",tab[i]);
    }

}

void remplirtab(int nb,int ch[]){
    for (int i=0;i<nb;i++){
        printf("donner un chiffre\n");
        scanf("%d",&ch[i]);
    }
}

void inversechiffre(char ch[],int nb){

     int y;
     char j;
    y = nb -1;
    for(int i=0;i<y;i++){
           j = ch[i];
           ch[i]= ch[y];
           ch[y]= j;
           y--;

        }
}
int longueurchaine(const char ch []){
   int i=0;
   while(ch[i]!='\0'){
        i++;
   }

    return i;
}

int chartoint(char c){
    if(c>47 && c<58){
        return c-= 48;
    }
    else if(c>64 && c<71){
        return c-= 55;
    }
    else if(c>96 && c<103){
        return c-= 87;
    }
}

char inttochar(int c){
    if(c>=0 && c<10){
        return c+= 48;
    }
    else if(c>9 && c<16){
        return c+= 55;
    }
}
void conversioninttodecimal(int nb,char tab[]){
    int i=0;
     while(nb!=0)
    {
        tab[i]=inttochar(nb%10);
        nb/=10;
        i++;
    }
    tab[i]='\0';
    inversechiffre(tab,longueurchaine(tab));
}

void conversioninttobinaire(int nb,char tab[]){
    int i=0;
     while(nb!=0)
    {
        tab[i]=inttochar(nb%2);
        nb/=2;
        i++;
    }
    tab[i]='\0';
    inversechiffre(tab,longueurchaine(tab));
}

void conversioninttohexadecimal(int nb,char tab[]){
     int i=0;
     while(nb!=0)
    {
        tab[i]=inttochar(nb%16);
        nb/=16;
        i++;
    }
    tab[i]='\0';
    inversechiffre(tab,longueurchaine(tab));

}

void conversioninttooctal(int nb,char tab[]){
   int i=0;
     while(nb!=0)
    {
        tab[i]=inttochar(nb%8);
        nb/=8;
        i++;
    }
    tab[i]='\0';
    inversechiffre(tab,longueurchaine(tab));

}


int conversionhexatoint (char tab[]){
    int resultat =0;
    inversechiffre(tab,longueurchaine(tab));
    for(int i = 0;i<longueurchaine(tab);i++){
        resultat += chartoint(tab[i])*power(16,i);
    }
    inversechiffre(tab,longueurchaine(tab));
    return resultat;
}

int conversionoctaltoint(char tab[]){
    char invch;
    int resultat =0;
    inversechiffre(tab,longueurchaine(tab));
    for(int i = 0;i<longueurchaine(tab);i++){
        resultat += chartoint(tab[i])*power(8,i);
    }
    inversechiffre(tab,longueurchaine(tab));
    return resultat;
}

int conversionbinairetoint(char tab[]){
    int resultat =0;
    inversechiffre(tab,longueurchaine(tab));
    for(int i = 0;i<longueurchaine(tab);i++){
        resultat += chartoint(tab[i])*power(2,i);
    }
    inversechiffre(tab,longueurchaine(tab));
    return resultat;

}

int conversiondecimaltoint (char tab[]){
    int resultat =0;
    inversechiffre(tab,longueurchaine(tab));
    for(int i = 0;i<longueurchaine(tab);i++){
        resultat += chartoint(tab[i])*power(10,i);
    }
    inversechiffre(tab,longueurchaine(tab));
    return resultat;

}


void sousmenu(int nb,char tabres[]){
    int choix;
    printf("dans quel base voulez vous convertir le nombre");
    printf("bienvenue dans le convertisseur sur quel base numerique voulez vous convertir :\n");
    printf("1)base binaire (base 2) \n");
    printf("2)base octale (base 8) \n");
    printf("3)base decimal (base 10) \n");
    printf("4)base hexadecimal (base 16) \n");
    scanf("%d",&choix);
    switch (choix){
        case 1 : printf("base binaire :\n");
            conversioninttobinaire(nb,tabres);
            printf("%s",tabres);
                break;
        case 2 : printf("base decimal :\n");
            conversioninttodecimal(nb,tabres);
            printf("%s",tabres);
                break;
        case 3 : printf("base octale :\n");
            conversioninttooctal(nb,tabres);
            printf("%s",tabres);
                break;
        case 4 : printf("base hexadecimal :\n");
            conversioninttohexadecimal(nb,tabres);
            printf("%s",tabres);
                break;

    }
}


int main()
{
char tab[N];
char tabres[N];
int choix='y';


while(choix=='y'){
system("cls");
printf("bienvenue dans le convertisseur sur quel base numerique voulez vous convertir :\n");
printf("1)base binaire (base 2) \n");
printf("2)base octale (base 8) \n");
printf("3)base decimal (base 10) \n");
printf("4)base hexadecimal (base 16) \n");
scanf("%d",&choix);

switch(choix){
    case 1 : printf("\nveuillez rentree votre nombre binaire :");
             scanf("%s",&tab);
             sousmenu(conversionbinairetoint(tab),tabres);
             break;

    case 3 : printf("\nveuillez rentree votre nombre decimal :");
             scanf("%s",&tab);
             sousmenu(conversiondecimaltoint(tab),tabres);
             break;

    case 2 : printf("\nveuillez rentree votre nombre octal :");
             scanf("%s",&tab);
             sousmenu(conversionoctaltoint(tab),tabres);
             break;

    case 4 : printf("\nveuillez rentree votre nombre hexadecimal :");
             scanf("%s",&tab);
               sousmenu(conversionhexatoint(tab),tabres);
             break;
}
printf("\n \nvoulez vous recommencer :\n");
scanf(" %s",&choix);
}

    return 0;
}
