<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/api", name="api_")
 */
class APIController extends AbstractController
{
    /**
     * @Route("/article/liste", name="liste", methods={"GET"})
     */
    public function liste(ArticleRepository $articleRepo)
    {
        // On recupére les articles
      $articles = $articleRepo->apiFindAll();
    //   dd($articles);
        // On specifie qu'on utilise un encoder en json
        $encoders =[new JsonEncoder()];

       // On instancie le "normaliseur" pour convertir la colection en tableau
       $normalizers = [new ObjectNormalizer()]; 

       // On fait la convertion en json
        // On instancie le convertisseur
        $serialiser = new Serializer($normalizers, $encoders);

        //On convertit en json
        // circular reference : est un element qui se pointe lui mème et en serialisant il se repointe lui meme se qui fait une boucle infini
        // c'est a dire si j'ai des articles dans des categories au lieu de m'afficher l'article globale il va vouloir affiche le meme article
        // mais celui qui est racrocher a la categorie
        // dans symfony 4 faut ajouté une function pour le circular reference,on mets un tableau d'option qui prend comme parametre 
        // la façon de comment utilisé et gérér la reference circulaire
        // lui si tu trouve une reference circulaire tu prend l'objet et tu me donne son id
        // avk 'circular_reference_handler'=>function($object){
        //return $object->getId();=> allé chercher l'id de la reference circulaire.
        //}
        $jsonContent = $serialiser->serialize($articles,'json');
        //dd($jsonContent);
        // on instancie la reponse de symfony
        $response = new Response($jsonContent);

        // on ajoiute l'entéte http
        $response->headers->set('Content-Type','application/json');

        // on envoie la reponse
        return $response;
    }
    /**
     * 
     * @Route("/article/lire/{id}", name="lire" , methods={"GET"})
     */
    public function getArticle(Article $article){
      
          // On specifie qu'on utilise un encoder en json
          $encoders =[new JsonEncoder()];
  
         // On instancie le "normaliseur" pour convertir la colection en tableau
         $normalizers = [new ObjectNormalizer()]; 
  
         // On fait la convertion en json
          // On instancie le convertisseur
          $serialiser = new Serializer($normalizers, $encoders);
  
          // On convertit en json
          // circular reference : est un element qui se pointe lui mème et en serialisant il se repointe lui meme se qui fait une boucle infini
          // c'est a dire si j'ai des articles dans des categories au lieu de m'afficher l'article globale il va vouloir affiche le meme article
          // mais celui qui est racrocher a la categorie
          // dans symfony 4 faut ajouté une function pour le circular reference,on mets un tableau d'option qui prend comme parametre 
          // la façon de comment utilisé et gérér la reference circulaire
          // lui si tu trouve une reference circulaire tu prend l'objet et tu me donne son id
          // avk 'circular_reference_handler'=>function($object){
          //return $object->getId();=> allé chercher l'id de la reference circulaire.
          //}
          $jsonContent = $serialiser->serialize($article,'json');
          //dd($jsonContent);
          // on instancie la reponse de symfony
          $response = new Response($jsonContent);
  
          // on ajoiute l'entéte http
          $response->headers->set('Content-Type','application/json');
  
          // on envoie la reponse
          return $response;
    }

    /**
     * ajout d'un article
     * 
     * @Route("/article/ajout", name="ajout", methods={"POST"})
     */
    public function addArticle(Request $request)
    {
        // On verifie si on a une request XMLHttpRequest
        // if($request->isXmlHttpRequest()){
            // On verifie les données apres les avoir décodées
            $données = json_decode($request->getContent());
            // on instancie un nouvel article
            $article = new Article();

            // On hydrate notre article
            $article->setTitle($données->title);
            $article->setContent($données->content);
            $article->setImagefile($données->imagefile);
            $article->setCreatedAt(new \dateTime()); 
            // pour recuperer l'utilisateur
            // $user = $this->getDctrine->getRepository(User::class)->find(2);
            // $article->setUsers($user);

            // on sauvegarde en base de données 
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            // On retourne la confirmation
            return new Response('ok', 201); 
        // }
        // return new Response('Erreur', 404);
    }

/**
 * Modifier un article
 * 
 * @Route("/article/edit/{id}", name= "editer", methods = {"PUT"})
 */

    public function editArticle(? Article $article, Request $request){
         // On verifie si on a une request XMLHttpRequest
        // if($request->isXmlHttpRequest()){
            // On verifie les données apres les avoir décodées
            $données = json_decode($request->getContent());

            $code = 200;

            // on verifie si on n'a pas d'article
            if(!$article){
                // on instancie un nouvel article
                $article = new Article();

                // on met le code 201
                $code = 201;
            }
             // On hydrate notre article
             $article->setTitle($données->title);
             $article->setContent($données->content);
             $article->setImagefile($données->imagefile);
             $article->setCreatedAt(new \dateTime()); 
             // pour recuperer l'utilisateur
             // $user = $this->getDctrine->getRepository(User::class)->find(2);
             // $article->setUsers($user);
 
             // on sauvegarde en base de données 
             $em = $this->getDoctrine()->getManager();
             $em->persist($article);
             $em->flush();
 
             // On retourne la confirmation
             return new Response('ok', $code); 
        // }  
        // return new Response('Erreur', 404);
    }
    
    /**
     * Supprimer un article
     * 
     * @Route("/article/delete/{id}", name ="delete", methods ={"DELETE"})
     */
    public function removeArticle(Article $article){
        $em = $this->getDoctrine()->getManager();
        $em->remove($article);
        $em->flush();
        return new Response('ok');
    }
    
}
