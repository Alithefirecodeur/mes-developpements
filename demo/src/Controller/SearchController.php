<?php

namespace App\Controller;

use App\Entity\Search;
use App\Form\SearchType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search")
     */
    public function index(Request $request)
    {
        $form = $this->createForm(SearchType::class);
        $search = $form->handleRequest($request);
       
        return $this->render('search/index.html.twig', [
            // 'controller_name' => 'SearchController',
            'search'=> $search,
            'Forme'=> $form->createview(),
        ]);
    }
}
