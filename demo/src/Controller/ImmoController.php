<?php

namespace App\Controller;

use App\Entity\Immobilier;
use App\Form\ImmobilierType;
use App\Repository\ImmobilierRepository;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\SearchArticleType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;




class ImmoController extends AbstractController
{

    /**
     * @Route("/immobiliers" , name="immobilliers")
     */
    public function index(ImmobilierRepository $repo, Request $request)
    {
        $immobilies = $repo->findAll();
        $form = $this->createForm(SearchArticleType::class);
        $search = $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $immobilies = $repo->search($search->get('mots')->getData()); 
        }
        return $this->render('immo/indexImmo.html.twig', [
            'controller_name' => 'ImmoController',
            'immobilies'=> $immobilies,
            'formS'=>$form->createView()
        ]);
    }

  

    /**
     * @Route("immobilier/create", name="new")
     *  
     */  
   public function create(Immobilier $immobilier= null, Request $request)
   {
       $immobilier = new Immobilier;
    //    dd($immobilier);
       $form = $this->createForm(ImmobilierType::class,$immobilier);
        $form->handleRequest($request);
    
       if($form-> isSubmitted() && $form->isValid()){
         if(!$immobilier->getId()){
             $immobilier->setCreatedAt(new \DateTime());
         }
        $uploadedFile = $form['image']->getData();
        $destination = $this->getParameter('images_directory'); 
        $originalFilename = pathinfo($uploadedFile->getClientOriginalName(),PATHINFO_FILENAME);
        $newFilename = $originalFilename.'-'.uniqId().'.'.$uploadedFile->guessExtension();
        $uploadedFile->move(
            $destination,
            $newFilename
        );
        
        $immobilier->setImage($newFilename);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($immobilier);
        $em->flush();

        return $this->redirectToRoute('immobilliers');
       }
        
       return $this->render('immo/create.html.twig',[
           'formImmo'=> $form->createView()
       ]);
   }

     /**
     * @Route("/immobilier/{id}", name="immobilier_show")
     */

    public function show(Immobilier $immobilier)
    {
        return $this->render('immo/show.html.twig',[
            'immobilier'=>$immobilier
        ]);
    }


   /**
    * @Route("/immobilier/{id}/edit" , name="edit")
    */
    public function edit(Immobilier $immobilier, Request $request,$id)
    {
        $form = $this->createForm(ImmobilierType::class,$immobilier);
        $form->handleRequest($request);
        if($form->isSubmitted()&& $form->isValid())
        {
            $uploadedFile = $form['image']->getData();
            $destination = $this->getParameter('images_directory');
            $originalFilename = pathinfo($uploadedFile->getClientOriginalName(),PATHINFO_FILENAME);
            // dd($originalFilename);
            $newFilename = $originalFilename.'-'.uniqid().'.'.$uploadedFile->guessExtension();
            $uploadedFile->move(
                $destination,
                $newFilename
            );
            $immobilier->setImage($newFilename);

            $em= $this->getDoctrine()->getManager();
            $em->persist($immobilier);
            $em->flush();

            return $this->render('immo/show.html.twig',[
                'immobilier'=>$immobilier
            ]);
        }
        return $this->render('immo/edit.html.twig',[
            'formImmo'=> $form->createView(),
        ]);
    }

    /**
     * @Route("immobilier/{id}/delete", name="destroy")
     * @return RedirectResponse
     */
    public function supprimer(Immobilier $immobilier): RedirectResponse {
        $em = $this->getDoctrine()->getManager();
        $em->remove($immobilier);
        $em->flush();
        return $this->redirectToRoute('immobilliers');
    }
}
