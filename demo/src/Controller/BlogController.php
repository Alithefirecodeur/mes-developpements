<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Form\ArticleType;
use App\Form\SearchArticleType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog")
     */
    public function index(ArticleRepository $repo, Request $request)
    {
         $articles = $repo->findAll();
        $form = $this->createForm(SearchArticleType::class);
        $search = $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $articles = $repo->search($search->get('mots')->getData()); 
        }
        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
            'articles'=>$articles,
            'formSe'=> $form->createview() 
        ]);
    }
    /**
     * @Route("/", name="home")
     */
    public function home(Request $request){
        $form = $this->createForm(SearchArticleType::class);
        $search = $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $search->get('mots')->getData();

        }
        return $this->render('blog/home.html.twig',[
            'title'=> "bienvenue sur l'appli",
            'formSe'=> $form->createview()
        ]);
    }
    /**
     * @Route("blog/new", name="blog_create")
     * 
     */
    public function form(Article $article=null,  Request $request): Response {
        
        $article = new Article();
        $form = $this->createForm( ArticleType::class, $article);
            $form->handleRequest($request); 
            if($form->isSubmitted() && $form->isValid()){
                if(!$article->getId()){
                    $article->setCreatedAt(new \dateTime());
                }  
                $uploadedFile =  $form['imagefile']->getData();
                $destination = $this->getParameter('images_directory');
                $originalFilename = pathinfo($uploadedFile->getClientOriginalName(),PATHINFO_FILENAME);
                $newFilename =$originalFilename.'-'.uniqid().'.'.$uploadedFile->guessExtension();
                $uploadedFile->move(
                    $destination,
                    $newFilename,
                ); 

                $article->setImagefile($newFilename);
                //   dd($article);
                $em = $this->getDoctrine()->getManager();
                $em->persist($article);
                $em->flush();
                return $this->redirectToRoute('blog_show',['id'=>$article->getId()]);
            }      
        
        return $this->render('blog/create.html.twig',[
            'formArticle'=> $form->createView(),
            // 'formSe'=> $form->createView()
        ]);
    }

    /**
     * @Route("/blog/{id}", name="blog_show")
     */
    public function show( Article $article){
        return $this->render('blog/show.html.twig',[
            'article'=>$article
        ]);
    }
    /**
     * @Route("blog/{id}/edit", name="article_edit")
     */
    public function edit(Article $article, Request $request): Response{
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
           $uploadedFile =  $form['imagefile']->getData();
           $destination = $this->getParameter('images_directory');
           $originalFilename = pathinfo($uploadedFile->getClientOriginalName(),PATHINFO_FILENAME);
           $newFilename =$originalFilename.'-'.uniqid().'.'.$uploadedFile->guessExtension();
           $uploadedFile->move(
               $destination,
               $newFilename,
           ); 
           $article->setImagefile($newFilename);
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();
            return $this->render('blog/show.html.twig',[
                'article'=>$article
            ]);
        }
        return $this->render('blog/edit.html.twig',[
            'formArticle'=>$form->createView()
        ]);  
    }
    /**
     * @Route("blog/{id}/delete", name="article_delete")
     * @return RedirectResponse
     */
    public function supprimer(Article $article): RedirectResponse {
        $em = $this->getDoctrine()->getManager();
        $em->remove($article);
        $em->flush();
        return $this->redirectToRoute('blog');
    } 
}
