<?php

namespace App;

use App\Http\Controllers\LigaController;

use Illuminate\Database\Eloquent\Model;

class Liga extends Model
{
    protected $fillable = ['id', 'equipe', 'jouer', 'gagner', 'perdu', 'butpour', 'butcontre', 'point'];
}
