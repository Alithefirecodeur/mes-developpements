<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PremierLeague extends Model
{
    protected $fillable = ['id', 'equipe_anglaise', 'jouer', 'gagner', 'perdu', 'butpour', 'butcontre', 'point'];
}
