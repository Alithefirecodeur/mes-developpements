<?php

namespace App\Http\Controllers;

use App\Bundesliga;
use Illuminate\Http\Request;

class BundesligaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bundesligas = Bundesliga::all();
        return view('/Bundesliga/bundes', compact('bundesligas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/Bundesliga/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'equipe_allemande' => 'required',
            'jouer' => 'required',
            'gagner' => 'required',
            'perdu' => 'required',
            'butpour' => 'required',
            'butcontre' => 'required',
            'point' => 'required',
        ]);
        $bundesliga = new Bundesliga();
        if ($bundesliga != null) {
            $bundesliga->equipe_allemande = request('equipe_allemande');
            $bundesliga->jouer = request('jouer');
            $bundesliga->gagner = request('gagner');
            $bundesliga->perdu = request('perdu');
            $bundesliga->butpour = request('butpour');
            $bundesliga->butcontre = request('butcontre');
            $bundesliga->point = request('point');
            $bundesliga->save();
            return redirect()->route('bundesliga.index')->with('success', 'l\'équipe a été ajouté');
        }
        return view('/Bundesliga/error');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bundesliga = Bundesliga::find($id);
        return view('/bundesliga/edit', compact('bundesliga'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'equipe_allemande' => 'required',
            'jouer' => 'required',
            'gagner' => 'required',
            'perdu' => 'required',
            'butpour' => 'required',
            'butcontre' => 'required',
            'point' => 'required',
        ]);
        $bundesliga = Bundesliga::find($id);
        if ($bundesliga != null) {
            $bundesliga->equipe_allemande = request('equipe_allemande');
            $bundesliga->jouer = request('jouer');
            $bundesliga->gagner = request('gagner');
            $bundesliga->perdu = request('perdu');
            $bundesliga->butpour = request('butpour');
            $bundesliga->butcontre = request('butcontre');
            $bundesliga->point = request('point');
            $bundesliga->save();
            return redirect()->route('bundesliga.index')->with('modifier', 'l\'équipe a été modifier');
        }
        return view('/bundesliga/error');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bundesliga = Bundesliga::find($id);
        if ($bundesliga != null) {
            $bundesliga->delete();
            return redirect()->route('bundeliga.index')->with('supprimer', 'l\'équipe a été supprimer');
        }
        return view('/bundesliga/error');
    }
}
