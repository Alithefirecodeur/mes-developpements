<?php

namespace App\Http\Controllers;

use App\Liga;
use Illuminate\Http\Request;

class LigaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ligas = Liga::all();
        return view('/Liga/liga', compact('ligas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('/Liga/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Liga $liga)
    {
        // validation before registration
        $request->validate([
            'equipe' => 'required',
            'jouer' => 'required',
            'gagner' => 'required',
            'perdu' => 'required',
            'butpour' => 'required',
            'butcontre' => 'required',
            'point' => 'required',
        ]);
        $liga = new Liga();
        if ($liga != null) {
            $liga->equipe = request('equipe');
            $liga->jouer = request('jouer');
            $liga->gagner = request('gagner');
            $liga->perdu = request('perdu');
            $liga->butpour = request('butpour');
            $liga->butcontre = request('butcontre');
            $liga->point = request('point');
            $liga->save();
            return redirect()->route('liga.index')->with('success', 'votre équipe a été ajouté');
        }
        return view('/Liga/error');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $liga = Liga::find($id);
        return view('Liga/edit', compact('liga'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'equipe' => 'required',
            'jouer' => 'required',
            'gagner' => 'required',
            'perdu' => 'required',
            'butpour' => 'required',
            'butcontre' => 'required',
            'point' => 'required',
        ]);
        $liga = Liga::find($id);
        if ($liga != null) {
            $liga->equipe = request('equipe');
            $liga->jouer = request('jouer');
            $liga->gagner = request('gagner');
            $liga->perdu = request('perdu');
            $liga->butpour = request('butpour');
            $liga->butcontre = request('butcontre');
            $liga->point = request('point');
            $liga->save();
            return redirect()->route('liga.index')->with('modifier', 'Votre équipe a été modifier');
        }
        return view('/Liga/error');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $liga = Liga::find($id);
        if ($liga != null) {
            $liga->delete();
            return redirect()->route('liga.index')->with('supprimer', 'Votre équipe a été supprimer');
        }
        return view('/Liga/error');
    }
}
