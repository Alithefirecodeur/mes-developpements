<?php

namespace App\Http\Controllers;

use App\Premierleague;
use Illuminate\Http\Request;

class PremierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $premiereleagues = PremierLeague::all();
        return view('/Premier/league', compact('premiereleagues'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/Premier/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'equipe_anglaise' => 'required',
            'jouer' => 'required',
            'gagner' => 'required',
            'perdu' => 'required',
            'butpour' => 'required',
            'butcontre' => 'required',
            'point' => 'required'
        ]);
        $premiereleague = new Premierleague();
        if ($premiereleague != null) {
            $premiereleague->equipe_anglaise = request('equipe_anglaise');
            $premiereleague->jouer = request('jouer');
            $premiereleague->gagner = request('gagner');
            $premiereleague->perdu = request('perdu');
            $premiereleague->butpour = request('butpour');
            $premiereleague->butcontre = request('butcontre');
            $premiereleague->point = request('point');
            $premiereleague->save();
            return redirect()->route('premiereleague.index')->with('success', 'Votre équipe a été ajouté');
        }
        return view('/Premier/error');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $premiereleague = Premierleague::find($id);
        return view('/Premier/edit', compact('premiereleague'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'equipe_anglaise' => 'required',
            'jouer' => 'required',
            'gagner' => 'required',
            'perdu' => 'required',
            'butpour' => 'required',
            'butcontre' => 'required',
            'point' => 'required'
        ]);
        $premiereleague = Premierleague::find($id);
        if ($premiereleague != null) {
            $premiereleague->equipe_anglaise = request('equipe_anglaise');
            $premiereleague->jouer = request('jouer');
            $premiereleague->gagner = request('gagner');
            $premiereleague->perdu = request('perdu');
            $premiereleague->butpour = request('butpour');
            $premiereleague->butcontre = request('butcontre');
            $premiereleague->point = request('point');
            $premiereleague->save();
            return redirect()->route('premiereleague.index')->with('modifier', 'l\'équipe a été modifier');
        }
        return view('/Premier/error');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $premiereleague = Premierleague::find($id);
        if ($premiereleague != null) {
            $premiereleague->delete($id);
            return redirect()->route('premiereleague.index')->with('supprimer', 'l\'équipe a été supprimer');
        }
        return view('/Premier/error');
    }
}
