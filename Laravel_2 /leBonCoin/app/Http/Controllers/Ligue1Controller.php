<?php

namespace App\Http\Controllers;

use App\Liguef;
use Illuminate\Http\Request;

class Ligue1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ligue1s = Liguef::all();
        return view('LigueFrance/ligue', compact('ligue1s'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/LigueFrance/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'equipe' => 'required',
            'jouer' => 'required',
            'gagner' => 'required',
            'perdu' => 'required',
            'butpour' => 'required',
            'butcontre' => 'required',
            'point' => 'required',
        ]);
        $ligue1 = new Liguef();
        if ($ligue1 != null) {
            $ligue1->equipe = request('equipe');
            $ligue1->jouer = request('jouer');
            $ligue1->gagner = request('gagner');
            $ligue1->perdu = request('perdu');
            $ligue1->butpour = request('butpour');
            $ligue1->butcontre = request('butcontre');
            $ligue1->point = request('point');
            $ligue1->save();
            return redirect()->route('ligue1.index')->with('success', 'votre équipe a été ajoutée');
        }
        return view('/LigueFrance/error');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ligue1 = Liguef::find($id);
        return view('/LigueFrance/edit', compact('ligue1'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'equipe' => 'required',
            'jouer' => 'required',
            'gagner' => 'required',
            'perdu' => 'required',
            'butpour' => 'required',
            'butcontre' => 'required',
            'point' => 'required',
        ]);
        $ligue1 = Liguef::find($id);
        if ($ligue1 != null) {
            $ligue1->equipe = request('equipe');
            $ligue1->jouer = request('jouer');
            $ligue1->gagner = request('gagner');
            $ligue1->perdu = request('perdu');
            $ligue1->butpour = request('butpour');
            $ligue1->butcontre = request('butcontre');
            $ligue1->point = request('point');
            $ligue1->save();
            return redirect()->route('ligue1.index')->with('success', ' l\'equipe du championnat français a été modifier');
        }
        return view('/LigueFrance/error');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ligue1 = Liguef::find($id);
        if ($ligue1 != null) {
            $ligue1->delete();
            return redirect()->route('ligue1.index')->with('supprimer', 'L\'equipe a été supprimer');
        }
        return view('/LigueFrance/error');
    }
}
