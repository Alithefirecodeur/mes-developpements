
   @extends('welcome')
    @section('content')
    <div class="container">
         <h2> Championnat de France</h2>
            <a href="{{ route('ligue1.create')}}" class="btn btn-success"> Ajouter une équipe</a>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">classement</th>
                            <th scope="col">équipe</th>
                            <th scope="col">MJ</th>
                            <th scope="col">MG</th>
                            <th scope="col">MP</th>
                            <th scope="col">BP</th>
                            <th scope="col">BC</th>
                            <th scope="col">Point</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                        @foreach ($ligue1s as $ligue1)
                            <tr>
                                <th>{{$ligue1->id}}</th>
                                <td>{{$ligue1->equipe}}</td>
                                <td>{{$ligue1->jouer}}</td>
                                <td>{{$ligue1->gagner}}</td>
                                <td>{{$ligue1->perdu}}</td>
                                <td>{{$ligue1->butpour}}</td>
                                <td>{{$ligue1->butcontre}}</td>
                                <td>{{$ligue1->point}}</td>
                                <td class="action">
                                   
                                     <a href="{{ route('ligue1.edit', ['ligue1'=>$ligue1])}}" class="btn btn-warning">Modifier</a> 
                                    <a> <form action="{{ route('ligue1.destroy', ['ligue1'=>$ligue1])}}"
                                        method="POST" enctype="multipart/form-data"
                                        onsubmit="return confirm('Voulez Vous vraiment supprimer ce magasin')" >
                                        @csrf
                                        @method('DELETE')
                                      
                                        <button class="btn btn-danger">Supprimer </button>
                                    </form></a>
                                     
                                </td>     
                            </tr>
                        @endforeach
                       
                </table>
    </div>            
    @endsection
    

