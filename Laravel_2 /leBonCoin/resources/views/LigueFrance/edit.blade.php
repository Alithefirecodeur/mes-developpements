@extends('welcome')
@section('content')
<div class="edition">
    <h3> Veuillez Modifier votre équipe</h3>
<form action="{{ route('ligue1.update',['ligue1'=>$ligue1])}}"
    method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Nom équipe</label>
        <input type="text" name="equipe" class="form-control"  placeholder="Modifier votre equipe" value="{{ $ligue1->equipe}}">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Match jouer</label>
        <input type="text" name="jouer" class="form-control" value="{{ $ligue1->jouer}}" placeholder="Modifier les matchs jouer">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Match gagner</label>
        <input type="text" name="gagner" class="form-control" value="{{ $ligue1->gagner}}" placeholder="Modifier les matchs gagner">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Match perdu</label>
        <input type="text" name="perdu" class="form-control" value="{{ $ligue1->perdu}}" placeholder="Modifier les matchs perdu">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">But pour</label>
        <input type="text" name="butpour" class="form-control" value="{{ $ligue1->butpour}}" placeholder="Modifier modifier les buts pour">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">But contre</label>
        <input type="text" name="butcontre" class="form-control" value="{{ $ligue1->butcontre}}" placeholder="Modifier les buts contre ">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Point</label>
        <input type="text" name="point" class="form-control" value="{{ $ligue1->point}}" placeholder="Modifier les points ">
    </div>
    <button type="submit" class="btn btn-success boutton">Modifier</button>
</form>
</div>
@endsection
