@extends('welcome')
@section('content')
<div class="edition">
    <h3> Veuillez Modifier votre équipe</h3>
<form action="{{ route('premiereleague.update', ['premiereleague'=>$premiereleague])}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Nom équipe</label>
        <input type="text" name="equipe_anglaise" class="form-control"  placeholder="Modifier votre equipe" value="{{ $premiereleague->equipe_anglaise}}">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Match jouer</label>
        <input type="text" name="jouer" class="form-control" value="{{ $premiereleague->jouer}}" placeholder="Modifier les matchs jouer">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Match gagner</label>
        <input type="text" name="gagner" class="form-control" value="{{ $premiereleague->gagner}}" placeholder="Modifier les matchs gagner">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Match perdu</label>
        <input type="text" name="perdu" class="form-control" value="{{ $premiereleague->perdu}}" placeholder="Modifier les matchs perdu">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">But pour</label>
        <input type="text" name="butpour" class="form-control" value="{{ $premiereleague->butpour}}" placeholder="Modifier modifier les buts pour">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">But contre</label>
        <input type="text" name="butcontre" class="form-control" value="{{ $premiereleague->butcontre}}" placeholder="Modifier les buts contre ">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Point</label>
        <input type="text" name="point" class="form-control" value="{{ $premiereleague->point}}" placeholder="Modifier les points ">
    </div>
    <button type="submit" class="btn btn-success boutton">Modifier</button>
</form>
</div>
@endsection
