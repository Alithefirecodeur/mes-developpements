@extends('welcome')
@section('content')
<div class="creat">
    <h3> Veuillez ajouter une équipe</h3>
<form action="{{ route('premiereleague.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Nom équipe</label>
        <input type="text" name="equipe_anglaise" class="form-control" placeholder="Taper le nom de l'équipe">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Match jouer</label>
        <input type="text" name="jouer" class="form-control" placeholder="mettez un nombre">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Match gagner</label>
        <input type="text" name="gagner" class="form-control" placeholder="mettez un nombre">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Match perdu</label>
        <input type="text" name="perdu" class="form-control" placeholder="mettez un nombre">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">But pour</label>
        <input type="text" name="butpour" class="form-control" placeholder="mettez un nombre">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">But contre</label>
        <input type="text" name="butcontre" class="form-control" placeholder="mettez un nombre">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Point</label>
        <input type="text" name="point" class="form-control" placeholder="mettez un nombre">
    </div>
    <button type="submit" class="btn btn-success">ajouter</button>
</form>
</div>
@endsection