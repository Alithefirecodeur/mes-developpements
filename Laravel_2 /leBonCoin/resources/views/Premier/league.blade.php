
   @extends('welcome')
   @section('content')
   <div class="container">
        <h2> Championnat Anglais</h2>
           <a href="{{route('premiereleague.create')}}" class="btn btn-success"> Ajouter une équipe</a>
               <table class="table table-hover">
                   <thead>
                       <tr>
                           <th scope="col">classement</th>
                           <th scope="col">équipe</th>
                           <th scope="col">MJ</th>
                           <th scope="col">MG</th>
                           <th scope="col">MP</th>
                           <th scope="col">BP</th>
                           <th scope="col">BC</th>
                           <th scope="col">Point</th>
                           <th scope="col">Action</th>
                       </tr>
                   </thead>
                       @foreach ($premiereleagues as $premiereleague)
                           <tr>
                               <th>{{$premiereleague->id}}</th>
                               <td>{{$premiereleague->equipe_anglaise}}</td>
                               <td>{{$premiereleague->jouer}}</td>
                               <td>{{$premiereleague->gagner}}</td>
                               <td>{{$premiereleague->perdu}}</td>
                               <td>{{$premiereleague->butpour}}</td>
                               <td>{{$premiereleague->butcontre}}</td>
                               <td>{{$premiereleague->point}}</td>
                               <td class="action">
                                  
                                    <a href="{{ route('premiereleague.edit',['premiereleague'=>$premiereleague])}}" class="btn btn-warning">Modifier</a> 
                                   <a> 
                                      <form action="{{ route('premiereleague.destroy',['premiereleague'=>$premiereleague])}}"
                                            method="POST" enctype="multipart/form-data"
                                            onsubmit="return confirm('Voulez Vous vraiment supprimer ce magasin')" >
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger">Supprimer </button>
                                       </form>
                                    </a>  
                               </td>     
                           </tr>
                       @endforeach
                      
               </table>
   </div>            
   @endsection
   

