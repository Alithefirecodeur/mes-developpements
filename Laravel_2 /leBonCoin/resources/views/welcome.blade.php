<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href={{ asset('csss/style.css') }}>
    <title>Mon bon Coin</title>
</head>

<body style="background: radial-gradient(#ebeff0 1px,#fff 0);">
    <header class="head">
        <div class="contain">
            <div class="row">
                <div class="logo col-4"> <a href="{{ route('welcome.index') }}">LebonAli</a> </div>
                <div class="devise col-8 ">Révons d'un Monde meilleurs</div>
            </div>
        </div>

        <div class=" ">
            <nav class="nav ">
                <a class="nav-link active" name="" href="{{ route('ligue1.index') }}">Ligue1</a>
                <a class="nav-link" name="bundes" href="{{ route('bundesliga.index')}}">Bundesliga</a>
                <a class="nav-link" name="liga" href="{{ route('liga.index') }}">Liga</a>
                <a class="nav-link" name="pr" href="{{route('premiereleague.index')}}">Premier League</a>
                <a class="nav-link" name="seriea" href="mode">Serie A</a>
                <a class="nav-link" name="ligap" href="mode">Liga Portugal</a>
                <a class="nav-link" name="ligap" href="mode">Autres Championnats</a>
            </nav>
            {{-- <div id="searchbar" class="col-2">
                    <form action="" class="champ">
                        <input type="button" value="">
                    <input id="searchbar" type="text" onkeyup="search_animal()" placeholder="rechercher" name="search">
                        
                    </form>
                </div> --}}
        </div>
    </header>
    @include('partials/alertespagne')
    @yield('content')
</body>

</html>
