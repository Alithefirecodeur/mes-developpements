
<div class="container">
    @if(session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @if(session('modifier'))
        <div class="alert alert-warning">
            {{ session('modifier') }}
        </div>
    @endif
    @if(session('supprimer'))
        <div class="alert alert-danger">
            {{ session('supprimer') }}
        </div>
    @endif
</div>


    