@extends('welcome')
@section('content')
<div class="container">
<h2> Classement Championnat espagnole</h2>
<a href="{{ route('bundesliga.create') }}" class="btn btn-success">ajouter une equipe</a>
<table class="table table-hover">
    <thead>
        <tr>
            <th scope="col">classement</th>
            <th scope="col">equipe</th>
            <th scope="col">MJ</th>
            <th scope="col">MG</th>
            <th scope="col">MP</th>
            <th scope="col">BP</th>
            <th scope="col">BC</th>
            <th scope="col">Point</th>
            <th>Action</th>
        </tr>
    </thead>
    @foreach($bundesligas as $bundesliga)
        <tbody class="col">

            <tr>
                <th scope="col">{{ $bundesliga->id }}</th>
                <td scope="col">{{ $bundesliga->equipe_allemande }}</td>
                <td scope="col">{{ $bundesliga->jouer }}</td>
                <td scope="col">{{ $bundesliga->gagner }}</td>
                <td scope="col">{{ $bundesliga->perdu }}</td>
                <td scope="col">{{ $bundesliga->butpour }}</td>
                <td scope="col">{{ $bundesliga->butcontre }}</td>
                <td scope="col">{{ $bundesliga->point }}</td>
                <td class="action"> <a
                        href="{{ route('bundesliga.edit',['bundesliga'=>$bundesliga]) }}"
                        class="btn btn-warning"> Modifier </a>
                    <a>
                        <form
                            action="{{ route('bundesliga.destroy',['bundesliga'=>$bundesliga]) }}"
                            method="POST" enctype="multipart/form-data"
                            onsubmit="return confirm('Voulez Vous vraiment supprimer ce magasin')" ;>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Supprimer</button>
                        </form>
                    </a>
                </td>
            </tr>
        </tbody>
    @endforeach
</table>
</div>
@endsection
