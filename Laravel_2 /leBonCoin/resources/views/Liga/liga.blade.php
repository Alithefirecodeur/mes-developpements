@extends('welcome')
@section('content')
<div class="container">
<h2> Classement Championnat espagnole</h2>
<a href="{{ route('liga.create') }}" class="btn btn-success">ajouter une equipe</a>
<table class="table table-hover">
    <thead>
        <tr>
            <th scope="col">classement</th>
            <th scope="col">equipe</th>
            <th scope="col">MJ</th>
            <th scope="col">MG</th>
            <th scope="col">MP</th>
            <th scope="col">BP</th>
            <th scope="col">BC</th>
            <th scope="col">Point</th>
            <th>Action</th>
        </tr>
    </thead>
    @foreach($ligas as $liga)
        <tbody class="col">

            <tr>
                <th scope="col">{{ $liga->id }}</th>
                <td scope="col">{{ $liga->equipe }}</td>
                <td scope="col">{{ $liga->jouer }}</td>
                <td scope="col">{{ $liga->gagner }}</td>
                <td scope="col">{{ $liga->perdu }}</td>
                <td scope="col">{{ $liga->butpour }}</td>
                <td scope="col">{{ $liga->butcontre }}</td>
                <td scope="col">{{ $liga->point }}</td>
                <td class="action"> <a
                        href="{{ route('liga.edit',['liga'=>$liga]) }}"
                        class="btn btn-warning"> Modifier </a>
                    <a>
                        <form
                            action="{{ route('liga.destroy',['liga'=>$liga]) }}"
                            method="POST" enctype="multipart/form-data"
                            onsubmit="return confirm('Voulez Vous vraiment supprimer ce magasin')" ;>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Supprimer</button>
                        </form>
                    </a>
                </td>
            </tr>
        </tbody>
    @endforeach
</table>
</div>
@endsection
