@extends('welcome')
@section('content')
<div class="edition">
    <h3>Veuillez modifier votre équipe</h3>
<form action="{{ route('liga.update',['liga'=>$liga]) }}"
    method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Nom équipe</label>
        <input type="text" name="equipe" class="form-control" value="{{ $liga->equipe }}">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Match jouer</label>
        <input type="text" name="jouer" class="form-control" value="{{ $liga->jouer }}">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Match gagner</label>
        <input type="text" name="gagner" class="form-control" value="{{ $liga->gagner }}">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Match perdu</label>
        <input type="text" name="perdu" class="form-control" value="{{ $liga->perdu }}">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">But pour</label>
        <input type="text" name="butpour" class="form-control" value="{{ $liga->butpour }}">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">But contre</label>
        <input type="text" name="butcontre" class="form-control" value="{{ $liga->butcontre }}">
    </div>
    <div class="mb-3 form group">
        <label for="exampleFormControlInput1" class="form-label">Point</label>
        <input type="text" name="point" class="form-control" value="{{ $liga->point }}">
    </div>
    <button type="submit" class="btn btn-success boutton">Modfier</button>
</form>
</div>
@endsection
