<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremierLeaguesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('premier_leagues', function (Blueprint $table) {
            $table->id();
            $table->string('equipe_anglaise');
            $table->integer('jouer');
            $table->integer('gagner');
            $table->integer('perdu');
            $table->integer('butpour');
            $table->integer('butcontre');
            $table->integer('point');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('premier_leagues');
    }
}
