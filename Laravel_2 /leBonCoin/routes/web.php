<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Ligue1Controller;
use App\Http\Controllers\LigaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome.index');
// Route for french championship
Route::resource('/ligue1', 'Ligue1Controller');

// Route for spanish chmpionship

Route::resource('/liga', 'LigaController');

// Route for England championship
Route::resource('/premiereleague', 'PremierController');

//Route for deutcheland championship
Route::resource('/bundesliga', 'BundesligaController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
